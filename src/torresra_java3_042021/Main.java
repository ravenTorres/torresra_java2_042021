
//QUESTIONS: 
//1. Write a Java function to sort a given array list.
//2. Write a Java function to rearrange an array list in a random order every time the function is called.
//3. Write a Java function that takes an ArrayList of integers as a parameter and moves the minimum value in the list to the front, while preserving the order of the elements. For example: [3,8,23,91,6,1] will turn to [1,3,8,23,91,6]
//4. Why should we opt for isEmpty() over size?
//5. Compare and contrast the classic for loop versus foreach. What are the pros and cons of both sides?
//6. Write a Java function that compares 2 sets. Return a set containing the 2 sets’ common values.
//7. Write a Java function that compares 2 sets and returns a set containing the values that are unique between the 2 sets.
//8. Write a Java function that counts the number of keys in a map that starts with “concordia”. E.g. {concordia1: “Value given”, acconcordia: “Test”, condensada: “Sweet”} - returns 1



package torresra_java3_042021;

import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        number1();
        number2();
        number3();
        number4();
        number5();
        number6();
        number7();
        number8();
    }
    
    //----- Function for number 1--------
    public static void number1(){
       System.out.println("----- NUMBER 1 -----");
       List<String> words = new ArrayList<String>();
       words.add("Torres");
       words.add("Raven");
       words.add("Soriano");
       
       System.out.println("Unsorted list: " + words);
       
       Collections.sort(words);
       System.out.println("Sorted list: " + words);
    }
    
    //----- Function for number 2--------
    public static void number2(){
       System.out.println("");
       System.out.println("----- NUMBER 2 -----");
       List<String> words = new ArrayList<String>();
       words.add("Torres");
       words.add("Raven");
       words.add("Soriano");
       words.add("Saver");
       words.add("21 years young");
       words.add("Cebu");
       words.add("CCT-Student");
       
       System.out.println("Unshuffled list: " + words);
       
       Collections.shuffle(words);
       System.out.println("Shuffled list:   " + words);
    }
     
    //----- Function for number 3--------
    public static void number3(){
       System.out.println("");
       System.out.println("----- NUMBER 3 -----");
       List<Integer> nums = new ArrayList<Integer>();
       nums.add(3);
       nums.add(8);
       nums.add(23);
       nums.add(91);
       nums.add(6);
       nums.add(1);
       
       int min = nums.get(0);
       for(int ndx = 1; ndx < nums.size(); ndx++) {
           if (nums.get(ndx) < min) {
               min = nums.get(ndx);
           }
       }
       
        System.out.println("Unmoved list: " + nums);
        
        int position = nums.indexOf(min);
        nums.remove(position);
        nums.add(0, min );
       
        System.out.println("Moved list:   " + nums);
    }
    
    //----- Function for number 4--------
    public static void number4(){
       System.out.println("");
       System.out.println("----- NUMBER 4 -----");
       System.out.println("We opt for isEmpty() over size because: \n  1. It is easier to read. \n  2. It checks both null and empty string.");
    }
    
    //----- Function for number 5--------
    public static void number5(){
       System.out.println("");
       System.out.println("----- NUMBER 5 -----");
       System.out.println("Classic for loop vs. for each \n");
       
       System.out.println(" CLASSIC FOR LOOP \n  1. The for loop is best for iterating over name-value pairs. \n  2. The for loop can be used to explore the possible properties and methods of an object.");
       System.out.println("    PROS: \n      -The advantage to a for loop is we know exactly how many times the loop will execute before the loop starts. \n");
       
       System.out.println(" FOR EACH \n  1. The forEach is loop best for iterating over values, for example arrays or objects.");
       System.out.println("    PROS: \n      -The possibility of programming error is eliminated. \n      -It makes the code more readable. \n      -There is no use of index or rather a counter in this loop.");
       System.out.println("    CONS: \n      -It cannot traverse through the elements in reverse fashion. \n      -You cannot skip any element as the concept of index is not there. \n      -You cannot choose to traverse to odd or even indexed elements too. ");
    }
    
    //----- Function for number 6--------
    public static void number6(){
        System.out.println("");
        System.out.println("----- NUMBER 6 -----");
        Set<String> firstSet = new HashSet<String>();
        firstSet.add("Apple");
        firstSet.add("Banana");
        firstSet.add("Lanzones");
        firstSet.add("Rambutan");
        firstSet.add("Orange");
  
        Set<String> secondSet = new HashSet<String>();

        secondSet.add("Strawberry");
        secondSet.add("Orange");
        secondSet.add("Watermelon");
        secondSet.add("Durian");
        secondSet.add("Apple");
  
        System.out.println("First Set:  " + firstSet);
        System.out.println("Second Set: " + secondSet);
        
        firstSet.retainAll(secondSet);
        System.out.println("Common Values: " + firstSet);

    }
    
    //----- Function for number 7 --------
    public static void number7(){
        System.out.println("");
        System.out.println("----- NUMBER 7 -----");
        Set<String> firstSet = new HashSet<String>();
        firstSet.add("Apple");
        firstSet.add("Banana");
        firstSet.add("Lanzones");
        firstSet.add("Rambutan");
        firstSet.add("Orange");
  
        Set<String> secondSet = new HashSet<String>();

        secondSet.add("Strawberry");
        secondSet.add("Orange");
        secondSet.add("Watermelon");
        secondSet.add("Durian");
        secondSet.add("Apple");
  
        System.out.println("First Set:  " + firstSet);
        System.out.println("Second Set: " + secondSet);
        
        Set<String> result = new HashSet<String>(firstSet);
        result.removeAll(secondSet);
        Set<String> result2 = new HashSet<String>(secondSet);
        result2.removeAll(firstSet);

       result.addAll(result2);
       System.out.println("Unique Set: " + result);

    }
    
    //----- Function for number 8--------
    public static void number8(){
        System.out.println("");
        System.out.println("----- NUMBER 8 -----");
        Map<String, String> word = new HashMap<String, String>();
        word.put("concardia1", "Game");
        word.put("concardialogy", "Nonsense");
        word.put("acconcardia", "Test");
        word.put("condensada", "sweet");

        Map<String, String> filteredMap = new HashMap<>();
        
        for (Map.Entry<String, String> entry: word.entrySet())
        {
            if (entry.getKey().startsWith("concardia")) {
                filteredMap.put(entry.getKey(), entry.getValue());
            }
        }
 
        System.out.println("Number of keys that starts with 'concardia':   " +filteredMap.size());
       
        
    }
}
